/*
package com.other;

import org.junit.Test;

import java.time.*;

*/
/**
 * @author ymsxyz
 * @version 1.0
 * @desc JDK8新增日期时间相关类
 * @date 2021/2/2 20:24
 *//*

public class TimeTest {

    */
/**
     * @return void
     * @desc JDK8新增  LocalDateTime类
     * @author ymsxyz
     * @date 2021/2/2 21:18
     *//*

    @Test
    public void test1() {
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);//2021-02-02T21:19:10.293782800

        LocalDateTime localDateTime = LocalDateTime.of(2020, 11, 11, 11, 11, 11);
        System.out.println(localDateTime);//2020-11-11T11:11:11
    }

    //localDateTime案例
    @Test
    public void test1_1() {
        LocalDateTime localDateTime = LocalDateTime.of(2020, 11, 11, 11, 11, 20);
        //public int getYear()           获取年
        int year = localDateTime.getYear();
        System.out.println("年为" + year);//年为2020
        //public int getMonthValue()     获取月份（1-12）
        int month = localDateTime.getMonthValue();
        System.out.println("月份为" + month);//月份为11

        Month month1 = localDateTime.getMonth();
//        System.out.println(month1);

        //public int getDayOfMonth()     获取月份中的第几天（1-31）
        int day = localDateTime.getDayOfMonth();
        System.out.println("日期为" + day);//日期为11

        //public int getDayOfYear()      获取一年中的第几天（1-366）
        int dayOfYear = localDateTime.getDayOfYear();
        System.out.println("这是一年中的第" + dayOfYear + "天");//这是一年中的第316天

        //public DayOfWeek getDayOfWeek()获取星期
        DayOfWeek dayOfWeek = localDateTime.getDayOfWeek();
        System.out.println("星期为" + dayOfWeek);//星期为WEDNESDAY

        //public int getMinute()        获取分钟
        int minute = localDateTime.getMinute();
        System.out.println("分钟为" + minute);//分钟为11
        //public int getHour()           获取小时

        int hour = localDateTime.getHour();
        System.out.println("小时为" + hour);//小时为11
    }

    //修改方法
    @Test
    public void test1_2() {
        //public LocalDateTime withYear(int year)   修改年
        LocalDateTime localDateTime = LocalDateTime.of(2020, 11, 11, 13, 14, 15);
        LocalDateTime newLocalDateTime = localDateTime.withYear(2048);
        System.out.println(newLocalDateTime);//2048-11-11T13:14:15

        //LocalDateTime newLocalDateTime = localDateTime.withMonth(20);//java.time.DateTimeException
        LocalDateTime newLocalDateTime1 = localDateTime.withMonth(12);//java.time.DateTimeException
        System.out.println(newLocalDateTime1);//2020-12-11T13:14:15
    }

    //JDK8  Period 计算时间(LocalDate)间隔
    @Test
    public void test1_3() {
        //public static Period between(开始时间,结束时间)  计算两个"时间"的间隔

        LocalDate localDate1 = LocalDate.of(2020, 1, 1);
        LocalDate localDate2 = LocalDate.of(2048, 12, 12);
        Period period = Period.between(localDate1, localDate2);
        System.out.println(period);//P28Y11M11D

        //public int getYears()         获得这段时间的年数
        System.out.println(period.getYears());//28
        //public int getMonths()        获得此期间的月数
        System.out.println(period.getMonths());//11
        //public int getDays()          获得此期间的天数
        System.out.println(period.getDays());//11

        //public long toTotalMonths()   获取此期间的总月数
        System.out.println(period.toTotalMonths());//347
    }

    //JDK8 计算两个“时间"(LocalDateTime)的间隔
    public void test1_4() {
        //public static Duration between(开始时间,结束时间)  计算两个“时间"的间隔

        LocalDateTime localDateTime1 = LocalDateTime.of(2020, 1, 1, 13, 14, 15);
        LocalDateTime localDateTime2 = LocalDateTime.of(2020, 1, 2, 11, 12, 13);
        Duration duration = Duration.between(localDateTime1, localDateTime2);
        System.out.println(duration);//PT21H57M58S
        //public long toSeconds()	       获得此时间间隔的秒
        System.out.println(duration.toSeconds());//79078
        //public int toMillis()	           获得此时间间隔的毫秒
        System.out.println(duration.toMillis());//79078000
        //public int toNanos()             获得此时间间隔的纳秒
        System.out.println(duration.toNanos());//79078000000000
    }
}
*/
