package com.other.Optional;

import org.junit.Test;

import java.util.Optional;

/**
 * @author ymsxyz
 * @version 1.0
 * @desc TODO
 * @date 2021/2/2 21:57
 */
public class OptionalTest {

    //私有学生内部类,只有外部类可访问
/*
    private class Student {
        private String name;
        private int age;

        public Student() {
        }

        public Student(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }

*/

    /**
     * @return void
     * @desc    public static <T> Optional<T> ofNullable(T value)
     *         获取一个Optional对象，Optional封装的值对象可以是null也可以不是null
     * @author ymsxyz
     * @date 2021/2/2 22:12
     */
    @Test
    public void method() {

        //Student s = new Student("zhangsan",23);
        Student s = null;
        //ofNullable方法，封装的对象可以是null，也可以不是null。
        Optional<Student> optional = Optional.ofNullable(s);

        System.out.println(optional);//Optional.empty
    }

    /**
     * @return void
     * @desc static <T> Optional<T> of(T value)    获取一个Optional对象，封装的是非null值的对象
     * Optional可以看做是一个容器，里面装了一个引用数据类型的对象。
     * 返回值就是Optional的对象
     * @author ymsxyz
     * @date 2021/2/2 22:10
     */
    @Test
    public void method1() {

/*
        Student s = null;
        Optional<Student> optional = Optional.of(s);//java.lang.NullPointerException
        System.out.println(optional);
*/

        Student s = new Student("zhangsan", 23);
        Optional<Student> optional = Optional.of(s);//
        System.out.println(optional);//Optional[com.other.Optional.OptionalTest$Student@7dc36524]
    }

}
