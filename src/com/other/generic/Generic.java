package com.other.generic;

import org.junit.Test;

/**
 * @author ymsxyz
 * @version 1.0
 * @desc 验证泛型是在编译阶段完成:通过查看形参t地址实现
 * @date 2021/2/3 13:44
 */
public class Generic {

    @Test
    public void test(){
        Generic g = new Generic();
        g.show("柳岩");//t="柳岩" value={byte[4]@1242}
        g.show(30);//t={Integer@1243} 30
        g.show(true);//t={Boolean@1244} true
        g.show(12.34);//t={Double@1245} 12.34
    }

    private  <T> void show(T t) {
        System.out.println(t);
    }

}