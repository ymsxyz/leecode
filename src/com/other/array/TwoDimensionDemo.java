package com.other.array;

import org.junit.Test;

/**
 * @author ymsxyz
 * @version 1.0
 * @desc TODO
 * @date 2021/2/2 21:00
 */
public class TwoDimensionDemo {

    /*
       问题: 二维数组中存储的是一维数组, 那能不能存入 [提前创建好的一维数组] 呢 ?
       答 : 可以的
    */
    public static void main(String[] args) {


    }

    /*
     * 问题: 二维数组中存储的是一维数组, 那能不能存入 [提前创建好的一维数组] 呢 ?
     * 答 : 可以的
     */
    @Test
    public void saveArray() {
        int[] arr1 = {11, 22, 33};
        int[] arr2 = {44, 55, 66};
        int[] arr3 = {77, 88, 99, 100};

        int[][] arr = new int[3][4];

        arr[2][3] = 101;
        System.out.println(arr[1][3]);//0 未赋值初始值为0
        System.out.println(arr[2][3]);//101

        arr[0] = arr1;
        arr[1] = arr2;
        arr[2] = arr3;

        System.out.println(arr[1][2]);//66
        System.out.println(arr[2][3]);//100
    }

    /*
     *  静态初始化:
     *  完整格式：数据类型[][] 变量名 = new 数据类型[][]{ {元素1, 元素2...} , {元素1, 元素2...} ...};
     *  简化格式: 数据类型[][] 变量名 = { {元素1, 元素2...} , {元素1, 元素2...} ...};
     */
    @Test
    public void staticInit() {
        int[] arr1 = {11, 22, 33};
        int[] arr2 = {44, 55, 66};

        int[][] arr = {{11, 22, 33}, {44, 55, 66}};
        System.out.println(arr[0][2]);

        int[][] array = {arr1, arr2};
        System.out.println(array[0][2]);
    }

}
