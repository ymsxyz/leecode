package com.other.thred.internet;

import java.net.InetAddress;
import java.net.UnknownHostException;

//基础
public class InetAddressDemo {
    public static void main(String[] args) {
        //InetAddress address = InetAddress.getByName("itheima");
        InetAddress address = null;
        try {
            address = InetAddress.getByName("192.168.1.104");
            //address = InetAddress.getByName("192.168.1.129");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        //public String getHostName()：获取此IP地址的主机名
        String name = address.getHostName();
        //public String getHostAddress()：返回文本显示中的IP地址字符串
        String ip = address.getHostAddress();

        System.out.println("主机名：" + name);
        System.out.println("IP地址：" + ip);
    }
}